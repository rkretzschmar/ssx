# Simple Secret Exchange

This is a very simple offline end-to-end encrypted secret exchange app using [Web Crypto API](https://developer.mozilla.org/en-US/docs/Web/API/Web_Crypto_API), [Create React App](https://github.com/facebook/create-react-app) and [Material-UI](https://material-ui.com/).

See a working version hosted as a Gitlab page at: https://rkretzschmar.gitlab.io/ssx/

Note: Unfortunately it is not running on IE or Microsoft Edge at the moment, even with the TextEncoding polyfill - see [7919579](https://developer.microsoft.com/en-us/microsoft-edge/platform/issues/7919579/) and [12782429](https://developer.microsoft.com/en-us/microsoft-edge/platform/issues/12782429/).
