export function hex(buffer: ArrayBuffer): string {
  const hexCodes = [];
  const view = new DataView(buffer);
  for (let i = 0; i < view.byteLength; i += 4) {
    // Using getUint32 reduces the number of iterations needed (we process 4 bytes each time)
    const value = view.getUint32(i);
    // toString(16) will give the hex representation of the number without padding
    const stringValue = value.toString(16);
    // We use concatenation and slice for padding
    const padding = "00000000";
    const paddedValue = (padding + stringValue).slice(-padding.length);
    hexCodes.push(paddedValue);
  }
  // Join all the hex strings into one
  return hexCodes.join("");
}

export async function sha256(str: string): Promise<string> {
  const buffer = new TextEncoder().encode(str);
  const hash = await crypto.subtle.digest("SHA-256", buffer);
  return hex(hash);
}

export async function generateKey(): Promise<CryptoKeyPair> {
  const key = await window.crypto.subtle.generateKey(
    {
      hash: { name: "SHA-256" },
      modulusLength: 4096,
      name: "RSA-OAEP",
      publicExponent: new Uint8Array([0x01, 0x00, 0x01])
    },
    true,
    ["encrypt", "decrypt"]
  );

  return key;
}

export async function exportKey(key: CryptoKey): Promise<JsonWebKey> {
  return await window.crypto.subtle.exportKey("jwk", key);
}

export async function importPublicKey(jwk: JsonWebKey): Promise<CryptoKey> {
  return await window.crypto.subtle.importKey(
    "jwk",
    jwk,
    {
      hash: { name: "SHA-256" },
      name: "RSA-OAEP"
    },
    false,
    ["encrypt"]
  );
}

export async function importPrivateKey(jwk: JsonWebKey): Promise<CryptoKey> {
  return await window.crypto.subtle.importKey(
    "jwk",
    jwk,
    {
      hash: { name: "SHA-256" },
      name: "RSA-OAEP"
    },
    false,
    ["decrypt"]
  );
}

export async function encryptMessage(
  message: string,
  publicKey: CryptoKey
): Promise<string> {
  try {
    // generate extractable AES message key
    const messageKey = await window.crypto.subtle.generateKey(
      { length: 256, name: "AES-GCM" },
      true,
      ["encrypt", "decrypt"]
    );

    // generate random init vector
    const iv = window.crypto.getRandomValues(new Uint8Array(12));

    // encode message
    const encodedMessage = new TextEncoder().encode(message);

    // encrypt message with generated AES message key and init vector
    const encryptedMessage = await window.crypto.subtle.encrypt(
      { iv, name: "AES-GCM" },
      messageKey,
      encodedMessage
    );

    // export the AES message key
    const exportedMessageKey = await exportKey(messageKey);

    // encrypt AES message key and init vector with public RSA key
    const messageKeyJSON = JSON.stringify({ k: exportedMessageKey.k, iv });
    const encodedMessageKeyJSON = new TextEncoder().encode(messageKeyJSON);
    const encrypedMessageKeyJSON = await window.crypto.subtle.encrypt(
      { name: "RSA-OAEP" },
      publicKey,
      encodedMessageKeyJSON
    );

    // return the message plus key JSON
    return JSON.stringify({
      k: new Uint8Array(encrypedMessageKeyJSON).toString(),
      m: new Uint8Array(encryptedMessage).toString()
    });
  } catch (e) {
    throw new Error(`Encryption wasn't possible because of: ${e}`);
  }
}

export async function decryptMessage(
  messagePlusKey: string,
  privateKey: CryptoKey
): Promise<string> {
  try {
    const messagePlusKeyObject = JSON.parse(messagePlusKey);

    const encodedMessagePlusKeyObject = new Uint8Array(
      messagePlusKeyObject.k
        .split(",")
        .map((item: string) => parseInt(item, 10))
    );

    // decrypt AES message key JSON
    const encodedMessageKeyJSON = await window.crypto.subtle.decrypt(
      { name: "RSA-OAEP" },
      privateKey,
      encodedMessagePlusKeyObject
    );

    // decode and parse AES message key JSON
    const messageKeyObject = JSON.parse(
      new TextDecoder().decode(encodedMessageKeyJSON)
    );

    // import AES message key
    const messageKey = await window.crypto.subtle.importKey(
      "jwk",
      {
        alg: "A256GCM",
        ext: true,
        k: messageKeyObject.k,
        kty: "oct"
      },
      "AES-GCM",
      false,
      ["encrypt", "decrypt"]
    );

    const iv = new Uint8Array(
      Object.keys(messageKeyObject.iv).map(k => messageKeyObject.iv[k])
    );

    // decrypt message with AES message key and init vector

    const decryptedMessage = await window.crypto.subtle.decrypt(
      {
        iv,
        name: "AES-GCM"
      },
      messageKey,
      new Uint8Array(
        messagePlusKeyObject.m
          .split(",")
          .map((item: string) => parseInt(item, 10))
      )
    );

    return new TextDecoder().decode(decryptedMessage);
  } catch (e) {
    throw new Error(`Decryption wasn't possible because of: ${e}`);
  }
}
