import Typography from "@material-ui/core/Typography";
import * as React from "react";

export default function() {
  return (
    <div>
      <Typography variant="body1">
        Hi, I'm Rene,<br />
        <br />as a freelance software developer I am always in the situation
        that I come into a new project and my colleagues have to share various
        secrets, such as passwords, API keys, etc. with me. Note and pen or USB
        stick are too cumbersome and error-prone for me, so I would like to use
        end-to-end encryption. Most colleagues do not use PGP and do not want to
        set it up. So what about WhatsApp, Signal, Threema and co.? To be
        honest, I don't want to get too private with most colleagues, at least
        not on the first working day and without knowing the person. So I would
        like to use a simple anonymous end-to-end encryption option.
        Unfortunately, my Google skills were not enough to find such a service
        on the Internet.
        <br />
        <br />So I implemented it myself and here it is.
        <br />
        <br />Have fun and best regards,
        <br />
        <br />Rene
      </Typography>
      <br />
      <Typography variant="title">Technology used</Typography>
      <br />
      <Typography variant="body1">
        I am using the{" "}
        <a href="https://developer.mozilla.org/en-US/docs/Web/API/Web_Crypto_API">
          Web Crypto API
        </a>{" "}
        that is supported by the most modern browsers.<br />
        <br />Messages are encrypted using a hybrid encryption approach. That
        means your message (your secret) is encrypted with a synchronous
        encryption algorithm. I used AES-GCM with a 256 bit key and a 12 byte
        init vector here. This key and the init vector are then encrypted with
        an asynchronous encryption algorithm, namely the public key of your
        conversation partner.<br />I used RSA-OAEP with a 4096 bit key and
        SHA-256 hash.
        <br />
        <br />
        View the code on{" "}
        <a href="https://gitlab.com/rkretzschmar/ssx">Gitlab</a>.
      </Typography>
    </div>
  );
}
