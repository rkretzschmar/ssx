import AppBar from "@material-ui/core/AppBar";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Tab from "@material-ui/core/Tab";
import Tabs from "@material-ui/core/Tabs";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import ContentCopy from "@material-ui/icons/ContentCopy";
import Mail from "@material-ui/icons/Mail";
import * as React from "react";
import * as CopyToClipboard from "react-copy-to-clipboard";
import AboutMe from "./AboutMe";
import * as Crypto from "./CryptoAPI";

export interface IState {
  privateKey?: CryptoKey;
  publicKey?: string;
  publicKeyHash?: string;
  error?: string;
  encrypted?: string;
  decrypted?: string;
  tab: number;
  messageRef: string;
  publicKeyRef: string;
  publicKeyRefHash?: string;
  encryptedMessageRef: string;
}

function sanitizePublicKey(s: string): string {
  return s.replace(/.*#/g, "");
}

class App extends React.Component<any, IState> {
  constructor(props: any) {
    super(props);
    this.state = {
      encryptedMessageRef: "",
      messageRef: "",
      publicKeyRef: "",
      tab: 2
    };
  }

  public componentDidMount() {
    if (
      window.location.hostname !== "localhost" &&
      window.location.protocol !== "https:"
    ) {
      window.location.protocol = "https:";
    } else {
      if (window.location.hash.length > 0) {
        this.setState(
          {
            publicKeyRef: sanitizePublicKey(window.location.hash),
            tab: 0
          },
          () => this.setPublicKeyHash(this.state.publicKeyRef)
        );
      }
      this.generateKey();
    }
  }

  public render() {
    const { tab } = this.state;
    return (
      <div
        style={{
          flexGrow: 1
        }}
      >
        <AppBar position="static">
          <Tabs centered={true} value={tab} onChange={this.onTabChange}>
            <Tab label="Encrypt" />
            <Tab label="Decrypt" />
            <Tab label="Your Public Key" />
            <Tab label="About" />
          </Tabs>
        </AppBar>
        <Grid
          container={true}
          style={{
            margin: "0 auto",
            maxWidth: 1024
          }}
        >
          <Grid item={true} xs={12}>
            {this.state.error && (
              <Typography style={{ color: "red", padding: 20 }}>
                {this.state.error}
              </Typography>
            )}
            <Typography
              style={{ marginTop: 20, marginLeft: 20 }}
              variant="headline"
            >
              Simple Secret Exchange
            </Typography>
            <Paper style={{ padding: 20, marginTop: 20 }}>
              {tab === 2 && (
                <div>
                  <div style={{ textAlign: "right" }}>
                    <Button color="secondary" onClick={this.regenerateKey}>
                      Generate new
                    </Button>
                  </div>
                  <br />
                  <Typography variant="title">Your Public Key</Typography>
                  <br />
                  <Typography>
                    Copy this key and send it together with the link to this
                    website to someone who wants to share a secret with you.
                    <br />
                    <br />
                    To be even more secure, send the fingerprint in a separate
                    email.
                    <br />
                    <br />
                    This public key is stored together with your private key in
                    your browser's local storage.
                  </Typography>
                  <br />
                  <div style={{ padding: 20, backgroundColor: "#eee" }}>
                    {this.state.publicKey && (
                      <div style={{ textAlign: "right", marginBottom: 20 }}>
                        <CopyToClipboard
                          text={
                            window.location.origin +
                            window.location.pathname +
                            "#" +
                            this.state.publicKey
                          }
                        >
                          <Button color="default" variant="raised">
                            <ContentCopy />
                            &nbsp;&nbsp;Copy to clipboard
                          </Button>
                        </CopyToClipboard>
                        <br />
                        <br />
                        <Button
                          color="default"
                          variant="raised"
                          href={`mailto:?subject=My public key&body=Send me a secret using this link: ${window
                            .location.origin +
                            window.location.pathname +
                            "#" +
                            this.state.publicKey}`}
                        >
                          <Mail />
                          &nbsp;&nbsp;Send via email
                        </Button>
                      </div>
                    )}
                    <Typography
                      variant="body1"
                      style={{
                        wordBreak: "break-all"
                      }}
                    >
                      {this.state.publicKey
                        ? `${this.state.publicKey}`
                        : "Is being generated ..."}
                    </Typography>
                    {this.state.publicKeyHash && (
                      <div style={{ textAlign: "right", marginBottom: 20 }}>
                        <Typography
                          variant="caption"
                          style={{
                            marginBottom: 20,
                            marginTop: 20,
                            wordBreak: "break-all"
                          }}
                        >
                          Fingerprint (SHA-256): {this.state.publicKeyHash}
                        </Typography>
                        <CopyToClipboard text={this.state.publicKeyHash}>
                          <Button color="default" variant="raised">
                            <ContentCopy />
                            &nbsp;&nbsp;Copy fingerprint to clipboard
                          </Button>
                        </CopyToClipboard>
                        <br />
                      </div>
                    )}
                  </div>
                </div>
              )}

              {tab === 0 && (
                <div>
                  <div style={{ textAlign: "right" }}>
                    <Button
                      color="secondary"
                      onClick={this.resetEncryptionForm}
                    >
                      Reset
                    </Button>
                  </div>
                  <br />
                  <Typography variant="title">The other public key</Typography>
                  <br />
                  <Typography>
                    If you want to share an encrypted secret with someone, you
                    must first ask them to visit this website and send you their
                    public key.
                    <br />
                    Then enter this public key here.
                  </Typography>
                  <br />
                  <TextField
                    multiline={true}
                    rowsMax={3}
                    style={{ width: "100%" }}
                    placeholder="Paste the other public key here"
                    value={this.state.publicKeyRef}
                    onChange={this.onPublicKeyChange}
                  />
                  {this.state.publicKeyRefHash && (
                    <Typography
                      variant="caption"
                      style={{ marginTop: 20, wordBreak: "break-all" }}
                    >
                      Fingerprint (SHA-256): {this.state.publicKeyRefHash}
                    </Typography>
                  )}
                  <br />
                  <br />
                  <Typography variant="title">
                    Your secret plain text message
                  </Typography>
                  <br />
                  <Typography>
                    Enter your secret here and press <i>Encrypt</i>
                    <br />
                    Note: Your secret, both unencrypted and encrypted, is never
                    transmitted to any server.
                    <br />
                    The encryption only takes place in your own browser.
                    <br />
                    To check this, you can switch off your network connection
                    before pressing the <i>Encrypt</i> button.
                  </Typography>
                  <br />
                  <TextField
                    multiline={true}
                    rowsMax={5}
                    placeholder="Enter your secret plain text message here"
                    style={{ width: "100%" }}
                    value={this.state.messageRef}
                    onChange={this.onMessageChange}
                  />
                  <br />
                  <br />
                  <Button
                    variant="outlined"
                    color="primary"
                    onClick={this.encryptMessage}
                  >
                    Encrypt
                  </Button>
                  <br />
                  {this.state.encrypted && (
                    <div>
                      <br />
                      <Typography variant="title">
                        The encrypted message
                      </Typography>
                      <br />
                      <Typography>
                        Copy this encrypted message and send it to the person
                        who sent you the public key entered above.
                      </Typography>
                      <br />
                      <div style={{ padding: 20, backgroundColor: "#eee" }}>
                        {this.state.encrypted && (
                          <div style={{ textAlign: "right", marginBottom: 20 }}>
                            <CopyToClipboard text={this.state.encrypted}>
                              <Button color="default" variant="raised">
                                <ContentCopy />
                                &nbsp;&nbsp;Copy to clipboard
                              </Button>
                            </CopyToClipboard>
                          </div>
                        )}

                        <Typography
                          variant="body1"
                          style={{
                            wordBreak: "break-all"
                          }}
                        >
                          {this.state.encrypted}
                        </Typography>
                      </div>
                    </div>
                  )}
                </div>
              )}
              {tab === 1 && (
                <div>
                  <div style={{ textAlign: "right" }}>
                    <Button
                      color="secondary"
                      onClick={this.resetDecryptionForm}
                    >
                      Reset
                    </Button>
                  </div>
                  <br />
                  <Typography variant="title">The encrypted message</Typography>
                  <br />
                  <Typography>
                    Insert here the encrypted message that someone sent you and
                    press <i>Decrypt</i>.<br />
                    Note: To decrypt the message, it must be encrypted with your
                    public key.
                  </Typography>
                  <br />
                  <TextField
                    multiline={true}
                    rowsMax={3}
                    placeholder="Paste the encrypted message you got here"
                    style={{ width: "100%" }}
                    value={this.state.encryptedMessageRef}
                    onChange={this.onEncryptedMessageChange}
                  />
                  <br />
                  <br />
                  <Button
                    variant="outlined"
                    color="primary"
                    onClick={this.decryptMessage}
                  >
                    Decrypt
                  </Button>
                  <br />
                  {this.state.decrypted && (
                    <div>
                      <br />
                      <Typography variant="title">
                        The decrypted message
                      </Typography>
                      <br />
                      <div
                        style={{
                          backgroundColor: "#eee",
                          padding: 20,
                          textAlign: "right"
                        }}
                      >
                        <CopyToClipboard text={this.state.decrypted}>
                          <Button color="default" variant="raised">
                            <ContentCopy />
                            &nbsp;&nbsp;Copy to clipboard
                          </Button>
                        </CopyToClipboard>

                        <TextField
                          multiline={true}
                          style={{ width: "100%" }}
                          value={this.state.decrypted}
                          disabled={true}
                        />
                      </div>
                    </div>
                  )}
                </div>
              )}
              {tab === 3 && <AboutMe />}
            </Paper>
          </Grid>
        </Grid>
      </div>
    );
  }

  private setPublicKeyHash = async (publicKeyRef: string) => {
    const publicKeyRefHash = await Crypto.sha256(publicKeyRef);
    this.setState({ publicKeyRefHash });
  };

  private onPublicKeyChange = (e: any) => {
    if (e.target === null) {
      return;
    }
    const publicKeyRef = sanitizePublicKey(e.target.value);
    this.setState({ publicKeyRef }, () => this.setPublicKeyHash(publicKeyRef));
  };

  private onMessageChange = (e: any) => {
    if (e.target === null) {
      return;
    }
    this.setState({ messageRef: e.target.value });
  };

  private onEncryptedMessageChange = (e: any) => {
    if (e.target === null) {
      return;
    }
    this.setState({ encryptedMessageRef: e.target.value });
  };

  private onTabChange = (event: any, tab: number) => {
    this.setState({ tab });
  };

  private regenerateKey = () => {
    window.localStorage.removeItem("key");
    this.setState(
      { publicKey: undefined, publicKeyHash: undefined },
      this.generateKey
    );
  };

  private generateKey = async () => {
    try {
      const keyString = window.localStorage.getItem("key");
      let publicKey: string;
      let privateKey: CryptoKey;
      if (keyString !== null) {
        const keyObject = JSON.parse(keyString);
        publicKey = btoa(JSON.stringify(keyObject.publicKey));
        privateKey = await Crypto.importPrivateKey(keyObject.privateKey);
      } else {
        const keyPair = await Crypto.generateKey();
        const keyObject = {
          privateKey: await Crypto.exportKey(keyPair.privateKey),
          publicKey: await Crypto.exportKey(keyPair.publicKey)
        };
        window.localStorage.setItem("key", JSON.stringify(keyObject));
        publicKey = btoa(JSON.stringify(keyObject.publicKey));
        privateKey = keyPair.privateKey;
      }
      const publicKeyHash = await Crypto.sha256(publicKey);
      this.setState({ publicKey, publicKeyHash, privateKey });
    } catch (e) {
      this.setState({ error: e.message });
    }
  };

  private encryptMessage = async () => {
    try {
      const publicKey = await Crypto.importPublicKey(
        JSON.parse(atob(this.state.publicKeyRef))
      );
      const encrypted = btoa(
        await Crypto.encryptMessage(this.state.messageRef, publicKey)
      );
      this.setState({ encrypted, error: undefined });
    } catch (e) {
      this.setState({ error: e.message });
    }
  };

  private resetEncryptionForm = () => {
    this.setState({
      encrypted: undefined,
      error: undefined,
      messageRef: "",
      publicKeyRef: "",
      publicKeyRefHash: undefined
    });
  };

  private decryptMessage = async () => {
    if (!this.state.privateKey) {
      return;
    }
    try {
      const decrypted = await Crypto.decryptMessage(
        atob(this.state.encryptedMessageRef),
        this.state.privateKey
      );
      this.setState({ decrypted, error: undefined });
    } catch (e) {
      this.setState({ error: e.message });
    }
  };

  private resetDecryptionForm = () => {
    this.setState({
      decrypted: undefined,
      encryptedMessageRef: "",
      error: undefined
    });
  };
}

export default App;
